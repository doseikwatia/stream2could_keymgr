#!/usr/local/bin/python

'''
Created on Mar 19, 2015

@author: daniel
'''
import argparse,logging
import os, getpass,paramiko,re
from subprocess import Popen,PIPE
from datetime import date, datetime
from scp import SCPClient
import ConfigParser
import hashlib,shutil


BASEDIR=os.path.dirname(__file__)

def main():
    #Setting command line parser up
    cmlparser=argparse.ArgumentParser()
    cmlparser.add_argument("-k", "--keyfile",required=True,
                  help="specify SSH private key to use")
    cmlparser.add_argument("-p", "--password", 
                         help="specify password with this option")
    cmlparser.add_argument("-H","--HOST",help="Specify Host IP address or hostname", 
                           required=True)
    cmlparser.add_argument("-v", "--verbose", choices=["DEBUG","INFO","WARNING","ERROR","CRITICAL"],
                           default="INFO")
    cmlparser.add_argument("-u","--username",help="Specify ssh username for remote host",required=True)
    args=cmlparser.parse_args()
    
    '''
Validating whatever the user inputed
    '''
    #Configuring logging
    
    loglevel={
              "DEBUG":logging.DEBUG,
              "INFO":logging.INFO,
              "WARNING":logging.WARNING,
              "ERROR":logging.ERROR,
              "CRITICAL":logging.CRITICAL
              }
    logging.basicConfig(format="%(asctime)s %(filename)s:line:%(lineno)d %(levelname)s %(message)s", # Setting log format
                        datefmt='%m/%d/%Y %H:%M:%S',                    # Date format used in logfile
                        filename=os.path.join(
                                              BASEDIR,
                                              "./logs/keymgr_"+date.today().strftime("%m%d%Y")+".log"),
                        level=loglevel[args.verbose])
    
    
    
    #Testing whether password was entered
    
    password=None
    if (args.password==None):
        msg="No password was passed as commandline argument, querying user for password"
        logging.debug(msg)
        password=getpass.getpass("Password: ")
    else:
        msg="Password was detected from commandline as an argument"
        logging.debug(msg)
        password=args.password
        
        
    
    #Validating the existence of file
    if not os.path.isfile(args.keyfile):
        msg="Specified keyfile, %s, does not exist" %args.keyfile
        logging.error(msg)
        print msg
        exit(2)
    msg ="Key file has been verfied to exist"
    logging.debug(msg)
    
    
    ipRe=re.compile(
                    '''
            ^                                       # Match start of string
            ([2][0-5][0-5]|^[1]{0,1}[0-9]{1,2})     # Match decimal octet
            \.                                      # Match '.' character
            ([0-2][0-5][0-5]|[1]{0,1}[0-9]{1,2})    # Match decimal octet
            \.                                      # Match '.' character
            ([0-2][0-5][0-5]|[1]{0,1}[0-9]{1,2})    # Match decimal octet
            \.                                      # Match '.' character
            ([0-2][0-5][0-5]|[1]{0,1}[0-9]{1,2})    # Match decimal octet
            $                                       # Match end of string
            ''', re.VERBOSE
                    )
    if not ipRe.match(args.HOST):
        msg="Invalid IP address entered"
        logging.error(msg)
        print msg
        #exit(3)
    logging.debug("Passed IP Address validation")
    #pinging address  p = Popen(cmd, stdout=PIPE, stderr=PIPE)
    proc=Popen(["ping", "-nc1", "%s" %args.HOST] ,stdout=PIPE, stderr=PIPE)
    out,err=proc.communicate()
    if( proc.poll() is not 0):
        msg="Remote host is not reacheable"
        logging.error(msg)
        msg=err
        logging.error(msg)
        print msg
        exit(4)
    msg="Remote host has been verified to be reacheable"
    logging.debug(msg)
    logging.debug(out)
    

    
    #Verifying key with the password
    key=None
    try:
        key=paramiko.RSAKey.from_private_key_file(args.keyfile,password)
        
        logging.info("SSH Key password passed")
    except paramiko.ssh_exception.SSHException as e:
        msg="Password Exception thrown"
        print msg
        logging.error(msg)
        exit(5)
    except:
        msg="An unknown exception has occurred"
        print msg
        logging.critical(msg)
        exit(6)
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               
    
    msg="All validations are successful. pulling old key from remote machine"
    logging.info(msg)
    print msg
    
    ssh=paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    try:
        ssh.connect(args.HOST, 
                    username=args.username, 
                    pkey=key, 
                    timeout=10,
                    #key_filename=args.keyfile,
                    #password=password,
                    allow_agent=False, 
                    look_for_keys=False)
        msg="Connected successfulling"
        logging.debug(msg)
    except paramiko.BadHostKeyException as e:
        msg="Bad host key exception occured"
        logging.error(msg)
        logging.error(e.message)
        exit(7)
    except paramiko.AuthenticationException as e:
        msg="Could not authenticate with remote Host"
        logging.error(msg)
        logging.error(e.message)
        exit(8)
    except paramiko.SSHException as e:
        msg="An unknown exception has occured"
        logging.error(msg)
        logging.error(e.message)
        exit(9)
    except:
        msg="Unknown error occured"
        logging.error(msg)
        exit(10)
        
        
    scp=SCPClient(ssh.get_transport())
    #Copying previous configuration into temporal file for manipulation
    #Loading stuff from script config file
    scriptConfig=ConfigParser.ConfigParser()
    scriptConfig.read("settings.cfg")
    
    localBackupBaseDir=scriptConfig.get("Local", "BackupDir")
    sensorBaseDir=scriptConfig.get("Sensor", "BaseDir")
    sensorConfigFile=scriptConfig.get("Sensor","Config")
    
    sensorTarget=os.path.join(sensorBaseDir,sensorConfigFile)
    
    #Setting tempfile up
    tempFile=os.path.join(BASEDIR,".tmp.txt")
    
    try:
        #Pulling config file from sensor
        scp.get(sensorTarget,tempFile)
    except:
        msg="Could not obtain the config file from remote host"
        print msg
        logging.error(msg)
        exit (11)
    
    
    
    
    
    
    sensorConfig=ConfigParser.ConfigParser()
    sensorConfig.read(tempFile)
    try:
        locationName=sensorConfig.get("location", "name")
        locationID=sensorConfig.getint("location","site")
        keyID=sensorConfig.getint("signing", "key_id")
    except:
        msg = "Could not parse config file pulled from sensor"
        print msg
        logging.error(msg)
        exit(13)
        
    
   # keydata="{:>3s}".format(locationName) + "{:>4s}".format(locationID) +"%03d"%(keyID+1)
    keydata="{:>3s}".format(locationName) + "%04d%03d"%(locationID,(keyID+1))
    newSensorKey=hash =  hashlib.pbkdf2_hmac('sha256', password, keydata, 10000, dklen=12)
    
    
    
    
    #modifying configuration
    sensorConfig.set("signing", "key_id", (keyID+1))
    sensorConfig.set("signing","key",newSensorKey.encode('hex'))
    
    
    
    
    #Today's directory
    nameDate=date.today().strftime("%m%d%Y")
#    nameDateTime=datetime.now().strftime("%m%d%Y%H%M%S")
    configNameTemplate="site_%s_%04d_%03d.config"
    localOldBackupFile=os.path.join(localBackupBaseDir,nameDate,"old",configNameTemplate %(locationName,locationID,keyID))
    localNewBackupFile=os.path.join(localBackupBaseDir,nameDate,"new",configNameTemplate %(locationName,locationID,(keyID+1)))
    
    # %(locationName,locationID,(keyID+1))
    if not os.path.isdir(os.path.join(localBackupBaseDir,nameDate)):
        msg="Backup directory for %s is created" %nameDate
        print msg
        logging.info(msg)
        os.makedirs(os.path.join(localBackupBaseDir,nameDate,"old"),0700)
        os.makedirs(os.path.join(localBackupBaseDir,nameDate,"new"),0700)
        
    
    msg="Moving files to backup directory"
    print msg
    logging.debug(msg)
    
    shutil.move(tempFile,localOldBackupFile)
    sensorConfig.write(open(localNewBackupFile,'wb'))
    
    try:
        scp.put(localNewBackupFile,sensorTarget)
    except:
        msg="Could not push file to Remote Host"
        print msg
        logging.error(msg)
        exit(14)
    
    ####Everything is successful####
    msg="Everything is successful"
    print msg
    logging.info(msg)
    '''
    [record]
format_id = 1

[location]
name = RI
site = 1

[signing]
key_id = 1
key = 61e0bffbed37f0d0c95f3ebf
    '''
    
    
    
    

if __name__ == '__main__':
    main()
#./main.py -k ~/test  -H 104.236.23.96 -v DEBUG -u daniel
